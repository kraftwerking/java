# Java Examples

## Miscelleneous Java classes

## Prerequisite

JDK 8 and Git must be installed in order to run the project locally.

## Cloning the repository and running tests

Clone the repository:

git clone [https://github.com/redmond007/java.git](https://github.com/redmond007/java.git)

Generally you can run a Java file by:

<Set classpath and any environmental variables>
javac <java file>
java <java file name>

**References**

[Java 8 API](https://docs.oracle.com/javase/8/docs/api/)
