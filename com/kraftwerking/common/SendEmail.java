package com.kraftwerking.common;

import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

public class SendEmail {
 public static void main(String[] args) {
  String host="localhost";
  final String user="me@myemailhost.com";
  final String password="xxxxx";
  String to="someone@gmail.com";
  //use properties to store mail server configuration
  //get the session object
   Properties properties = System.getProperties();
   properties.setProperty("mail.smtp.host", host);
   Session session = Session.getDefaultInstance(properties);

   //compose the message
    try {
      //javax.mail.internet.MimeMessage extends the javax.mail.Message abstract classes
      //allows you to compose a message
     MimeMessage message = new MimeMessage(session);
     //set message properties
     message.setFrom(new InternetAddress(user));
     message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));
     message.setSubject("Hi this is just a test");
     message.setText("This is a test email message");
    //send the message
     Transport.send(message);
     System.out.println("message sent successfully...");
     } catch (MessagingException e) {e.printStackTrace();}
 }
}
