package com.kraftwerking.common;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.File;
import java.io.FileInputStream;
import org.apache.commons.io.IOUtils;

public class ByteArrayParser {

    public static void main(String[] args) {

        //convert file object to byte array
        byte[] bytearr  = null;
        InputStream inputStream = null;
        try {
            File file = new File("sample.csv");
            inputStream = new FileInputStream(file);
            bytearr = IOUtils.toByteArray(inputStream);
            System.out.println("done converting file to bytearray");
            inputStream.close();
        } catch (java.lang.Exception exception) {
            exception.printStackTrace();
        }


        //convert byteArray to a BufferedReader to parse file
        InputStream is = null;
        try {
            is = new ByteArrayInputStream(bytearr);
            BufferedReader bfReader = new BufferedReader(new InputStreamReader(is));
            String temp = null;
            while ((temp = bfReader.readLine()) != null) {
                System.out.println(temp);
            }
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}