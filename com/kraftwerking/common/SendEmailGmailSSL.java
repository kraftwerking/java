package com.kraftwerking.common;

import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

public class SendEmailSSL{
 public static void main(String[] args) {
     //use properties to store mail server configuration
     Properties props = new Properties();
     props.put("mail.smtp.host", "smtp.gmail.com");
     props.put("mail.smtp.socketFactory.port", "465");
     props.put("mail.smtp.socketFactory.class",
               "javax.net.ssl.SSLSocketFactory");
     props.put("mail.smtp.auth", "true");
     props.put("mail.smtp.port", "465");
     //creates the session object that stores all the information of host like host name, username, password etc.
     Session session = Session.getDefaultInstance(props,
      new javax.mail.Authenticator() {
      protected PasswordAuthentication getPasswordAuthentication() {
      return new PasswordAuthentication("me@gmail.com","password");
      }
     });
     //compose message
     try {
      MimeMessage message = new MimeMessage(session);
      message.addRecipient(Message.RecipientType.TO,new InternetAddress("to@gmail.com"));
      message.setSubject("Hi this is a test message");
      message.setText("this is a test message");
      //send message
      Transport.send(message);
      System.out.println("message sent successfully");
     } catch (MessagingException e) {throw new RuntimeException(e);}
 }
}
